// src/screens/TeamManagement/TeamManagementScreen.tsx
import React from 'react';
import { View, Text } from 'react-native';
import FormComponent from '../../components/FormComponent/FormComponent';

const TeamManagementScreen: React.FC = () => {
  const handleTeamFormSubmit = (formData: any) => {
    // Lógica para enviar el formulario de equipo al backend
    console.log('Formulario de equipo enviado:', formData);
  };

  return (
    <View>
      <Text>Gestión de Equipos</Text>
      <FormComponent onSubmit={handleTeamFormSubmit} />
      {/* Otros elementos de la pantalla de gestión de equipos */}
    </View>
  );
};

export default TeamManagementScreen;

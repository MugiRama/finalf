// src/screens/UserManagement/UserManagementScreen.tsx
import React from 'react';
import { View, Text } from 'react-native';
import FormComponent from '../../components/FormComponent/FormComponent';

const UserManagementScreen: React.FC = () => {
  const handleUserFormSubmit = (formData: any) => {
    // Aquí puedes manejar la lógica para enviar el formulario de usuario al backend
    console.log('Formulario de usuario enviado:', formData);
  };

  return (
    <View>
      <Text>Gestión de Usuarios</Text>
      <FormComponent onSubmit={handleUserFormSubmit} />
      {/* Otros elementos de la pantalla de gestión de usuarios */}
    </View>
  );
};

export default UserManagementScreen;

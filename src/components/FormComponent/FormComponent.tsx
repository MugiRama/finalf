// src/components/FormComponent/FormComponent.tsx
import React, { useState, ChangeEvent } from 'react';
import { View, TextInput, Button } from 'react-native';

interface FormData {
  name: string;
  // Agrega otros campos según sea necesario
}

interface FormComponentProps {
  onSubmit: (formData: FormData) => void;
}

const FormComponent: React.FC<FormComponentProps> = ({ onSubmit }) => {
  const [formData, setFormData] = useState<FormData>({ name: '' });

  const handleInputChange = (key: keyof FormData, value: string) => {
    setFormData((prevData) => ({ ...prevData, [key]: value }));
  };

  const handleSubmit = () => {
    // Puedes agregar validaciones u otras lógicas antes de enviar el formulario
    onSubmit(formData);
  };

  return (
    <View>
      <TextInput
        placeholder="Nombre"
        onChangeText={(value) => handleInputChange('name', value)}
      />
      {/* Otros campos de formulario según tus necesidades */}
      <Button title="Enviar" onPress={handleSubmit} />
    </View>
  );
};

export default FormComponent;

// src/components/ViewComponent.tsx
import React from 'react';
import { Text, View } from 'react-native';

const ViewComponent = () => {
  // Puedes agregar lógica adicional aquí si es necesario
  return (
    <View>
      <Text>Hola mundo, ovalle</Text>
    </View>
  );
};

export default ViewComponent;
 